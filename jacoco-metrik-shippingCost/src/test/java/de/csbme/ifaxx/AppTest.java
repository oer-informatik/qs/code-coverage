package de.csbme.ifaxx;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.*;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    void testShippingCosts() {
        //given
        ShippingPosition[] positions = {new ShippingPosition(3, 0.5)};
        
        
        //when
        double result = App.shippingCalculator(positions);
        
        //then
        double expected = 5.2; // 3.9 + 3*0.1 + 1.0
        double delta = 0.001;    // Rundungsgenauigkeit: Schwankung 0.1ct sind ok
        assertEquals(expected, result, delta);
    }

    @Test
    void testShippingCostsLight() {
        //given
        ShippingPosition[] positions = {new ShippingPosition(2, 0.2)};
        
        
        //when
        double result = App.shippingCalculator(positions);
        
        //then
        double expected = 4.4; // 3.9 + 0.3 + 0.2
        double delta = 0.001;    // Rundungsgenauigkeit: Schwankung 0.1ct sind ok
        assertEquals(expected, result, delta);
    }


    
     @Test
    void testShippingCostsHeavy() {
        //given
        ShippingPosition[] positions = {new ShippingPosition(1, 11)};
        
        
        //when
        double result = App.shippingCalculator(positions);
        
        //then
        double expected = 50; // Sperrgut
        double delta = 0.001;    // Rundungsgenauigkeit: Schwankung 0.1ct sind ok
        assertEquals(expected, result, delta);
    } 

    @Test
    void testShippingManyPositions() {
        //given
        ShippingPosition[] positions = {new ShippingPosition(500, 0.001)};
        
        
        //when
        double result = App.shippingCalculator(positions);
        
        //then
        double expected = 50; // Sperrgut
        double delta = 0.001;    // Rundungsgenauigkeit: Schwankung 0.1ct sind ok
        assertEquals(expected, result, delta);
    } 

}
