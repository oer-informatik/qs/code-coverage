package de.csbme.ifaxx;

/**
 * Hello world!
 *
 */
public class App {

    public static double shippingCalculator(ShippingPosition[] positions){

        double shippingCost = 3.9;
        double totalWeight = 0;

        for (ShippingPosition pos : positions) {
            
            shippingCost += pos.getQuantity()*0.1; // 10ct pro Artikel

            if (pos.getQuantity()*pos.getWeight() < 0.5){ // Kleinartikel
                shippingCost += 0.3;
            }else if (pos.getQuantity()*pos.getWeight() < 2.5){ //größer
                shippingCost += 1.0;
            }

            totalWeight += pos.getQuantity()*pos.getWeight();

            if ((totalWeight >10) || (shippingCost >50)){  //Sperrgut
                shippingCost = 50;
            }
        }
        return shippingCost;
    }



    public static void main(String[] args) {
            }

}

class ShippingPosition{
    double price;
    int quantity;
    int ean;
    double weight;
    ShippingPosition(int quantity, double weight){
        this.quantity= quantity;
        this.weight = weight;
    }
    public double getWeight(){
        return weight;
    }
    public double getQuantity(){
        return quantity;
    }
}
