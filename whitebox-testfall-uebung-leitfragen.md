## Leitfragen zur Testfallerstellung nach Whitebox-Systematik (Überdeckungstests)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>


<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111523080351425661</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/whitebox-testfall-uebung-leitfragen</span>


> **tl/dr;** _(ca. 10 min Bearbeitungszeit): Die Leitfragen umfassen Grundlagen zur Testfallerstellung nach Whitebox-Systematik (Überdeckungstests) und Vergleiche der unterschiedlichen Überdeckungsmetriken zum Überprüfen des Grundwissens._

Die Aufgaben beziehen sich auf die Inhalte der folgenden Blogposts:

* [Whitebox-Testmetriken](https://oer-informatik.de/codecoverage)


### Leitfragen Whitebox-Tests:

1) Was versteht man unter Whitebox-Tests? <button onclick="toggleAnswer('wb1')">Antwort</button>

<span class="hidden-answer" id="wb1">
Bei Whitebox-Tests handelt es sich um Tests, die gegen die Implementierung erstellt werden. Whitebox-Testfälle werden so erstellt, dass die Testsuite einen möglichst großen Bereich des Codes erreicht.
</span>

2) Welche Systematiken können zur Generierung von Whitebox-Testfällen genutzt werden? <button onclick="toggleAnswer('wb2')">Antwort</button>

<div class="hidden-answer" id="wb2">
Für Whitebox-Tests werden Überdeckungsmetriken herangezogen. Es wird dabei überprüft, wie viel Prozent der vorhandenen 

- Zeilen, 

- Anweisungen, 

- Zweige, 

- Pfade oder 

- Bedingungen 

durch die Testfälle erreicht wird. Häufig helfen Unittest-Frameworks mit Codeabdeckungstools bei der Berechnung und Visualisierung der nicht abgedeckten Codeabschnitte.
</div>

3) Welche Aussagen können aus den Überdeckungs-Metriken abgeleitet werden? <button onclick="toggleAnswer('wb3')">Antwort</button>

<div class="hidden-answer" id="wb3">
Aus Überdeckungsmetriken können Aussagen über die Güte der Testsuite abgeleitet werden: Werden wenige Anweisungen oder Zweige durch die Tests erreicht, so ist die Wahrscheinlichkeit größer, dass sich in den nicht erreichten Anweisungen/Zweigen Fehler befinden. 

Eine umgekehrte Aussage ist nicht möglich: auch eine Funktion, in der alle Überdeckungsgrade 100% betragen, kann voller logischer Fehler sein.
</div>

4) Eine Testsuite für eine Funktion verfügt über Testfälle mit 100% Anweisungsüberdeckung. Wann und warum kann es sinnvoll sein, diese Testsuite mit Testfällen nach Blackbox-Systematik zu ergänzen? <button onclick="toggleAnswer('wb4')">Antwort</button>

<span class="hidden-answer" id="wb4">
Fehler passieren oft am Rand des Definitionsbereichs oder bei unerwarteten oder fehlerhaften Eingaben. Mit der Grenzwertanalyse bieten Blackbox-Testfälle eine Systematik, die dabei hilft, solche Szenarien mit in die Testsuite aufzunehmen.
</span>

5) "Verfügt eine Testsuite über 100% Zweigabdeckung, so ist automatisch auch die Anweisungsüberdeckung, Bedingungsüberdeckung und die Pfadüberdeckung bei 100% gegeben." Bewerte diese Aussage! <button onclick="toggleAnswer('wb5')">Antwort</button>

<div class="hidden-answer" id="wb5">
Zweigüberdeckung setzt voraus, dass an allen bedingten Anweisungen alle Zweige durchlaufen wurden. Das beinhaltet, dass alle (erreichbaren) Anweisungen auch durchlaufen wurden. Sofern also kein "toter Code" im getesteten Modul vorhanden ist (nicht erreichbare Zeilen, etwa `if (false) {nichtErreichbar()}`^[Die Spitzfindigen werden gemerkt haben: da die Bedingung auch in keinem Testdurchlauf `true` sein kann, kann für `if (false) {nichtErreichbar()}` auch keine 100% Zweigüberdeckung errreicht werden.]) folgt aus einer Zweigüberdeckung von 100% auch eine Anweisungsüberdeckung.

Die Pfadüberdeckung wiederum basiert auf der Kombination unterschiedlicher Zweige. Eine Pfadüberdeckung von 100% würde zwar die Zweigüberdeckung beinhalten. Umgekehrt gilt dies aber nicht.

Da die Bedingungsüberdeckung ein Spezialfall der Zweigüberdeckung ist, folgt aus 100% Bedingungsüberdeckung zwar 100% Zweigüberdeckung (und damit 100% Anweisungsüberdeckung), aber nicht umkehrt. Die vollständige Bedingungsüberdeckung muss also nicht gegeben sein.
</div>

6) Für eine Methode liegen Testfälle mit 100% Pfadüberdeckung vor. Welche Aussagen kannst Du daraus für Bedingungsüberdeckung und Zweigüberdeckung treffen? <button onclick="toggleAnswer('wb6')">Antwort</button>

<span class="hidden-answer" id="wb6">
Bei 100% Pfadabdeckung ist eine Anweisungs- und Zweigüberdeckung von 100% gegeben. Die Bedingungsüberdeckung betrifft jedoch zusätzlich zu den Zweigen noch Teilbedingungen, die ggf. bei 100% Pfadüberdeckung noch nicht 100% abgedeckt sind.
</span>

7) Für eine andere Methode liegen Testfälle mit 100% Anweisungsüberdeckung vor. Welche Aussagen kannst Du daraus für Bedingungsüberdeckung und Zweigüberdeckung treffen? <button onclick="toggleAnswer('wb7')">Antwort</button>

<span class="hidden-answer" id="wb7">
Aus 100% Anweisungsüberdeckung folgt weder Zweig- und Bedingungsüberdeckung. Leere Zweige müssen z.B. nicht unbedingt durchlaufen sein, um 100% Anweisungsüberdeckung zu erreichen.
</span>

### Weitere Übungsaufgaben

Links zu weiteren Übungsaufgaben finden sich über das Menü oder am Ende des [Artikels zu Whitebox-Tests/Code-Coverage](https://oer-informatik.de/codecoverage)
