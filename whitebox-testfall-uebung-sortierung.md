## Übungsaufgaben zur Testfallerstellung nach Whitebox-Systematik

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>


<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111523080351425661</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/whitebox-testfall-uebung-sortierung</span>


> **tl/dr;** _(ca. 45 min Bearbeitungszeit): Zu einer gegebenen Funktion (ein Sortieralgorithmus) soll ein Kontrollflussgraph erstellt werden, die McCabe-Zahl berechnet werden (beides optional), die Überdeckungsmetriken für einen bestehenden Test berechnet werden, weitere Tests aus Whitebox-Sicht ergänzt werden und alles mit einem Code-Coverage-Tool implementiert werden._

Die Aufgaben beziehen sich auf die Inhalte der folgenden Blogposts:

* [Whitebox-Testmetriken (CodeCoverage)](https://oer-informatik.de/codecoverage)

* [Metrik der Komplexität: McCabe-Zahl](https://oer-informatik.de/zyklomatische-komplexitaet)

### Aufgabe Überdeckungstests Sortieralgorithmus

Im Folgenden ist eine Beispielimplementierung für einen Sortier-Algorithmus abgebildet:

```java
public static int[] sortiere(int[] sortieren) {
  for (int i = 0; i < sortieren.length - 1; i++) {
    for (int j = i + 1; j < sortieren.length; j++) {
      if (sortieren[i] > sortieren[j]) {
        int temp = sortieren[i];
        sortieren[i] = sortieren[j];
        sortieren[j] = temp;
      }
    }
  }
  return sortieren;
}
```

a)	Erstelle für diese Methode einen Kontrollflussgraphen! <button onclick="toggleAnswer('wba3')">Antwort</button>

  <span class="hidden-answer" id="wba3">
  Hinweis: in dieser Beispiellösung heißt die FUnktion anderes, der Kontrollflussentwurf passt aber.
  ![Kontrollflussgraph mit 9 Knoten](images/uebungsaufgaben/kontrollflussgraph1.png)
  </span>

b)	Berechne die Zyklomatische Komplexität (McCabe-Zahl)! <button onclick="toggleAnswer('wba4')">Antwort</button>

  <div class="hidden-answer" id="wba4">
  Die McCabe-Zahl $M$ lässt sich am einfachsten über die Anzahl der binären Verzweigungen ($b$) berechnen: im Kontrollflußgraph oben haben die Knoten 2, 5 und  7 je zwei ausgehende Kanten ($b=3$)

  $M = b + 1 = 4$
  
  Hintergrund: [Artikel zur McCabe-Zahl](https://oer-informatik.de/zyklomatische-komplexitaet)
  </div>

c)	Welche Testfälle erhälst Du aus Blackbox-Sicht für diesen Algorithmus?
Erstelle für jede Blackbox-Systematik zwei Testfälle und dokumentiere die erforderlichen Informationen für die Testfälle tabellarisch! <button onclick="toggleAnswer('wba5')">Antwort</button>

  <div class="hidden-answer" id="wba5">
  |Testfall<br/>Nr. | Beschreibender Name der Testklasse / des Testfalls | Vor-<br/>bedingungen | Eingabewerte<br/> (Parameter `sortieren`) | Erwartetes<br/>Resultat | Nach-<br/>bedingungen | Tatsächliches<br/>Resultat | bestanden <br/>/ nicht bestanden
  --- | --- | --- |--- | --- | --- |--- | ---
  |1. | ÄK: Unsortiertes Array | keine |`sortiere([2;3;1])`| [1;2;3] | - |??| ??|
  |2. | ÄK: bereits sortiertes Array | keine |`sortiere([3;4;5])`| [3;4;5] | - |??| ??|
  |3. | GW: Leeres Array | keine |`sortiere([])`| [] | - |??| ??|
  |4. | GW: Ungültige Werte | keine |`sortiere(NULL)`| Exception | - |??| ??|
  </div>


d)	Es liegt nur der folgende Testfall vor:

  ```java
  assert-equals(sortiere([3;2;1]); [1;2;3]);
  ```

  Berechne den Anweisungsüberdeckungsgrad und den Zweigüberdeckungsgrad! Gibt die Werte in der Form `xx von yy` an (oder als nicht gekürzter Bruch), und nicht als Prozentzahl! <button onclick="toggleAnswer('wba6')">Antwort</button>

  <span class="hidden-answer" id="wba6">
  Anhand des Kontrollflussgraphen - am besten selbst nach zeichnen, alle durchlaufenen Anweisungen und Kanten kennzeichnen:<br/>
  ![Kontrollflussgraph mit 9 Knoten](images/uebungsaufgaben/kontrollflussgraph2.png)<br/>
  Anweisungsüberdeckung: $C_0=\frac{9}{9}=100\%$ <br/>
  Über Zweige: Zweig 7-6 wird nicht durchlaufen, <br/>
  Zweigüberdeckung: $C_1=\frac{5}{6}=100%$
  Über Kanten: Kante 7-6 wird nicht durchlaufen, <br/>
  Kantenüberdeckung: $C_1=\frac{10}{11} =100%$
  </span>

e) Übernimm die Methode in eine IDE und erstelle die entworfenen Testfälle. Überprüfe Deine Ergebnisse mit JaCoCo. 

### Weitere Übungsaufgaben

Links zu weiteren Übungsaufgaben finden sich über das Menü oder am Ende des [Artikels zu Whitebox-Tests/Code-Coverage](https://oer-informatik.de/codecoverage)
