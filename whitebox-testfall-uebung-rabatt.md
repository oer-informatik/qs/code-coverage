## Whiteboxtest-Übungsaufgabe "Rabatt"

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>


<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111523080351425661</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/whitebox-testfall-uebung-rabatt</span>


> **tl/dr;** _(ca. 15 min Bearbeitungszeit): Übungsaufgabe zur Whitebox-Testfall-Erstellung und Berechnung der Codeüberdeckung am Beispiel einer "Rabatt"-Funktion._

Die Aufgaben beziehen sich auf die Inhalte der folgenden Blogposts:

* [Whitebox-Testmetriken](https://oer-informatik.de/codecoverage)

* [Metrik der Komplexität: McCabe-Zahl](https://oer-informatik.de/zyklomatische-komplexitaet)


### Überdeckungstests-Übungsaufgabe Rabattrechner

Gegeben ist die folgende Methode, die Rabatte errechnet in Abhängigkeit der Positionskosten (über 100€ -> 5€ Rabatt) und Gesamtkosten (über 1000€ -> 5% Rabatt auf die rabattierte Gesamtsumme):

```java
    public static int rabattAusgeben(int[] kosten){
        int rabatt = 0;
        int summe = 0;
        for(int position:kosten){
            if (position > 100) {
                rabatt += 5;
            }
            summe += position;
        }
        if (summe-rabatt > 1000){
            rabatt +=(int)((summe-rabatt)*0.05);
        }
        return rabatt;
    }
```

a)	Welchen Anweisungsüberdeckungsgrad und Zweigüberdeckungsgrad erhältst Du mit dem  folgenden Testfall (Gibt die Werte in der Form `xx von yy` an (oder als nicht gekürzter Bruch), und nicht als Prozentzahl!):

```java
    @Test
    public void rabattAusgeben_1000_10_Test() {
        // given: Preparation
        int[] kosten = { 1000, 10 };

        // when: Execution
        int result = App.rabattAusgeben(kosten);

        // then: Verification
        int expected = 55;
        assertEquals(result, expected);
    }
```
<button onclick="toggleAnswer('wba1')">Antwort</button>

<div class="hidden-answer" id="wba1">
Testfall im Kontrollflußgraph: `1->2->3->4->5->6->3->4->6->3->7->8->9`

![Ausgabe von JaCoCo: C0: 100%, C1= 83%](images/uebungsaufgaben/kontrollflussgraph.png)<br/>

Anweisung 1 + 2 kann zusammengefasst werden.

C0 = durchlaufende Anweisungen / mögliche Anweisungen = 9/9 = 100% (bei Zusammenfassung von 1+2: 8/8 = 100%)

C1 = durchlaufende Zweige / mögliche Zweige = 5/6  = (83%)

Berechnung über Kanten statt Zweige: 

C1 = durchlaufende Kanten / mögliche Kanten = 9/10 = 90%

![Ausgabe von JaCoCo: C0: 100%, C1= 83%](images/uebungsaufgaben/jacoco-ausgabe.png)

</div>


b)	Ergänze so wenige Testfälle wie möglich, um eine 100%ige Zweigüberdeckung zu erhalten. (Erwartete Ergebnisse sind nicht erforderlich – lediglich die zu testenden Parameter sollen angegeben werden. Die Testfälle können abgekürzt werden nach dem Muster `assertEquals(result, expected)`, für den obigen Testfall also - weil das erwartete Resultat ja nicht berechnet werden muss, z.B.: `assertEquals(rabattAusgeben({ 1000, 10 }), ??);`)

<button onclick="toggleAnswer('wba2')">Antwort</button>

<span class="hidden-answer" id="wba2">
$summe-rabatt$ muss unter 1000 bleiben, damit else-Zweig genommen wird, also:<br/>
Testfall im Kontrollflußgraph: 1->2->3->4->6->3->7->9<br/>
`assertEquals(rabattAusgeben({10 }), ??);`
</span>

### Weitere Übungsaufgaben

Links zu weiteren Übungsaufgaben finden sich über das Menü oder am Ende des [Artikels zu Whitebox-Tests/Code-Coverage](https://oer-informatik.de/codecoverage)
