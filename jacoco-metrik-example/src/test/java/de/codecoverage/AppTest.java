package de.codecoverage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.*;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void rabattAusgeben_100_Test() {
        // given: Preparation
        int[] kosten = { 100 };

        // when: Execution
        int result = App.rabattAusgeben(kosten);

        // then: Verification
        int expected = 0;
        assertEquals(expected, result);
    }


    @Test
    public void rabattAusgeben_500_Test() {
        // given: Preparation
        int[] kosten = { 500 };

        // when: Execution
        int result = App.rabattAusgeben(kosten);

        // then: Verification
        int expected = 5;
        assertEquals(expected, result);
    }

    @Test
    public void rabattAusgeben_NULL_Test() {
        // given: Preparation
        int[] kosten = {  };

        // when: Execution
        int result = App.rabattAusgeben(kosten);

        // then: Verification
        int expected = 0;
        assertEquals(expected, result);
    }

    @Test
    public void rabattAusgeben_1100_Test() {
        // given: Preparation
        int[] kosten = { 1100 };

        // when: Execution
        int result = App.rabattAusgeben(kosten);

        // then: Verification
        int expected = 59;
        assertEquals(expected, result);
    }

    @Test
    public void rabattAusgeben_1100_karte_Test() {
        // given: Preparation
        int[] kosten = { 1100 };
        boolean kartevorhanden = true;

        // when: Execution
        int result = App.rabattAusgeben(kosten, kartevorhanden);

        // then: Verification
        int expected = 59;
        assertEquals(expected, result);
    }

    @Test
    public void rabattAusgeben_1100_nokarte_Test() {
        // given: Preparation
        int[] kosten = { 1100 };
        boolean kartevorhanden = false;

        // when: Execution
        int result = App.rabattAusgeben(kosten, kartevorhanden);

        // then: Verification
        int expected = 5;
        assertEquals(expected, result);
    }

    @Test
    public void rabattAusgeben_100_nokarte_Test() {
        // given: Preparation
        int[] kosten = { 100 };
        boolean kartevorhanden = false;

        // when: Execution
        int result = App.rabattAusgeben(kosten, kartevorhanden);

        // then: Verification
        int expected = 0;
        assertEquals(expected, result);
    }

    @Test
    public void rabattAusgeben_100_karte_Test() {
        // given: Preparation
        int[] kosten = { 100 };
        boolean kartevorhanden = false;

        // when: Execution
        int result = App.rabattAusgeben(kosten, kartevorhanden);

        // then: Verification
        int expected = 0;
        assertEquals(expected, result);
    }
}
