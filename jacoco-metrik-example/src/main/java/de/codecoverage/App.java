package de.codecoverage;

/**
 * Hello world!
 *
 */
public class App {

    public static int rabattAusgeben(int[] kosten) {
        int rabatt = 0;
        int summe = 0;
        for (int position : kosten) {
            System.out.println("Position: "+ position);
            if (position > 100) {
                rabatt += 5;
                System.out.println("5€ Rabatt für "+ position);
            }
            summe += position;
        }
        
        System.out.println("Summe: "+ summe);
        System.out.println("Rabatte: "+ rabatt);
        System.out.println("Summe-Rabatt: "+(summe-rabatt) );
        if (summe - rabatt > 1000) {
            rabatt += (int) ((summe - rabatt) * 0.05);
        }
        return rabatt;
    }

    public static int rabattAusgeben(int[] kosten, boolean rabattkarteVorhanden){
        int rabatt = 0;
        int summe = 0;
        for(int position:kosten){
            if (position > 100) {
                rabatt += 5;
            }
            summe += position;
        }
        if ((summe-rabatt > 1000)&&(rabattkarteVorhanden == true)){
            rabatt +=(int)((summe-rabatt)*0.05);
        }
        return rabatt;
    }


    public static void main(String[] args) {
        int[] kosten = { 100 };
        System.out.println(rabattAusgeben(kosten));
    }

}
