## Übungsaufgaben zur Testfallerstellung

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109977598496135624</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/testfall-uebungsaufgaben</span>


> **tl/dr;** _Beispielfragen rund um Testgetriebene Entwicklung (_Testdriven Development_, TDD), Testfallerstellung aus Whitebox- und Blackbox-Systematik._

Die Übungsaufgaben wurden in zwei Bereiche aufgeteilt. Die Aufgaben mit Bezug zu **Blackbox / Testgetriebener Entwicklung**, also zu den Blogposts:

* [Testgetriebene Entwicklung (mit pytest)](https://oer-informatik.de/python_einstieg_pytest)

* [Blackbox-Testfallerstellung](https://oer-informatik.de/blackboxtests)


...finden sich in den Dokumenten:


- [Leitfragen zur Blackbox-Systematik](https://oer-informatik.de/blackbox-testfall-uebung-leitfragen)

- [Leitfragen zur Testgetriebenen Entwicklung (TDD)](https://oer-informatik.de/tdd-leitfragen)

- [kleine Blackboxtest-Übungsaufgabe "Punkte im String"](https://oer-informatik.de/blackbox-testfall-uebung-punkteImString)

- [kleine Blackboxtest-Übungsaufgabe "Klassengröße"](https://oer-informatik.de/blackbox-testfall-uebung-klassengroesse)

- [kleine Blackboxtest-Übungsaufgabe "Ganzzahliges Teilen"](https://oer-informatik.de/blackbox-testfall-uebung-ganzzahligesTeilen)

- [größere Blackboxtest-Übungsaufgabe "IHK-Abschlussnote"](https://oer-informatik.de/blackbox-testfall-uebung-berechnungIHKAbschluss)

- [größere Blackboxtest-Übungsaufgabe "Verzinsung"](https://oer-informatik.de/blackbox-testfall-uebung-verzinsung)


Die Übungsaufgaben mit Bezug zu **Whitebox / Überdeckungstests**, also zu den Blogposts:

* [Whitebox-Testmetriken](https://oer-informatik.de/codecoverage)

* [Metrik der Komplexität: McCabe-Zahl](https://oer-informatik.de/zyklomatische-komplexitaet)

...finden sich in den Artikeln:

- [Leitfragen zur Whitebox-Systematik](https://oer-informatik.de/whitebox-testfall-uebung-leitfragen)

- [Whitebox-Übungsaufgaben "ShippingCost"](https://oer-informatik.de/whitebox-testfall-uebung-shippingcost)

- [Whitebox-Übungsaufgaben "Rabatt"](https://oer-informatik.de/whitebox-testfall-uebung-rabatt)

- [Whitebox-Übungsaufgaben "Sortierung"](https://oer-informatik.de/whitebox-testfall-uebung-sortierung)

