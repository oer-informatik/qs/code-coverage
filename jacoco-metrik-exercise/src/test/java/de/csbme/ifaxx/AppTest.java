package de.csbme.ifaxx;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.*;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void allZero() {
        // given: Preparation
        Double[] gasConsumption = {0.0}; 
        Double[] temperatureOutside = {20.0};
        long startDayIndex = 0;
        long dayAmount = 1;
        double temperatureInside = 18;
        double temperatureHeatinglimit= 14;

        // when: Execution
        double result = App.calculateHeatingDemand(gasConsumption, temperatureOutside, startDayIndex, dayAmount, temperatureInside, temperatureHeatinglimit);

        // then: Verification
        int expected = 0;
        assertEquals(expected, result);
    }

    @Test
    public void noGasConsumption() {
        // given: Preparation
        Double[] gasConsumption = {0.0}; 
        Double[] temperatureOutside = {20.0};
        long startDayIndex = 0;
        long dayAmount = 1;
        double temperatureInside = 18;
        double temperatureHeatinglimit= 14;

        // when: Execution
        double result = App.calculateHeatingDemand(gasConsumption, temperatureOutside, startDayIndex, dayAmount, temperatureInside, temperatureHeatinglimit);

        // then: Verification
        int expected = 0;
        assertEquals(expected, result);
    }
}
